extern crate anyhow;

use anyhow::Result;
use askama::Template;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

use crate::render::{CodeFile, LineOfCode, Project};

mod render;

fn main() -> Result<()> {
    let code_files = find_files("./src")?
        .into_iter()
        .filter(|p| p.is_file())
        .map(|f| read_file(f))
        .collect::<Result<Vec<CodeFile>>>()?;
    println!("{}", Project { files: code_files }.render().unwrap());
    Ok(())
}

fn read_file(path: PathBuf) -> Result<CodeFile> {
    let name = path.file_name();
    let lines = read_lines(&path)?;
    let lines = lines
        .into_iter()
        .map(|r| {
            r.map(|l| LineOfCode {
                code: l,
                comments: vec![],
            })
        })
        .map(|r| r.map_err(|e| e.into()))
        .collect::<Result<Vec<LineOfCode>>>()?;
    return Ok(CodeFile {
        fileName: name.and_then(|s| s.to_str()).unwrap_or("ERROR").to_string(),
        linesOfCode: lines,
    });
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn find_files(dir: &str) -> Result<Vec<PathBuf>> {
    WalkDir::new(dir)
        .into_iter()
        .map(|r| r.map(|d| d.into_path()))
        .map(|r| r.map_err(|e| e.into()))
        .collect()
}
