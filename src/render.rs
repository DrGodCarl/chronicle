use askama::Template;

pub struct LineOfCode {
    pub code: String,
    pub comments: Vec<String>,
}

pub struct CodeFile {
    pub fileName: String,
    pub linesOfCode: Vec<LineOfCode>,
}

#[derive(Template)]
#[template(path = "code.html.j2")]
pub struct Project {
    pub files: Vec<CodeFile>,
}
